import Editor from '../../src/editor';

describe('Editor Tests', ()=> {

    let _transformsServiceMock;
    let _res;
    beforeEach(()=> {

        jest.unmock('../../src/components/TransformsService');
        _transformsServiceMock = require('../../src/components/TransformsService').default;
        _transformsServiceMock.prototype.Scale = jest.fn();
        _transformsServiceMock.prototype._scale = jest.fn();
        _transformsServiceMock.prototype._scale.mockImplementation(()=> { _res = 1 });
        
    });

    it('should initialize the editor with components', ()=> {
        const _editor = new Editor();
        expect(Object.keys(_editor.componentList).length).toBeGreaterThan(0);
    });

    it('should instigate a test event in the Transforms Service', ()=> {
        const _editor = new Editor();
        _editor.Drag();
        expect(_transformsServiceMock.prototype.Scale).toHaveBeenCalled();
       _res = undefined;
    });

});
