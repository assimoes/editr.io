import Snap from 'snapsvg-cjs';
import Mediator from '../mediator';
import { Components } from '../components';

export default class Editor {

    constructor(container) {
        this.mediator = new Mediator();
        this.components = {};

        this.container = Snap(container);
        let elements = this.container.selectAll('.snap-elem');
        
        this.store = {
            container: this.container,
            elements: { }
        };

        elements.forEach((elem)=> {
            let bbox = elem.getBBox();

            elem.node.addEventListener('click', ()=> {
                this.mediator.publish('element_selected', elem);
            });

            this.store.elements[elem.id] = {
                transforms : {
                    rotate : 0,
                    translate: {
                        x: 0,
                        y: 0
                    },
                    scale: {
                        scaleX: 1,
                        scaleY: 1,
                    },
                    center:{x: bbox.cx, y: bbox.cy},
                    size:{
                        x: bbox.width,
                        y: bbox.height
                    }
                },
                copy: {},
                sin: -1,
                cos: -1,
                element: elem,
                bbox: bbox
            }
        });

        for (let component in Components) {
            this.components[component] = new Components[component](Snap,this.mediator, this.store);
        }
    }

    Drag() {
        this.mediator.publish('dragging');
    }

    get Store() {
        return this.store;
    }
    
    get Mediator() {
        return this.mediator;
    }

    get componentList() {
        return this.components;
    }

}