const Snap = require('snapsvg-cjs');

var s = Snap("#svg");

var templatePath = document.getElementById("rect");
var path = s.select("#"+templatePath.id);

var bbox = path.getBBox();

const transforms = {
    rotate : 0,
    translate: {
        x: 0,
        y: 0
    },
    scale: {
        scaleX: 1,
        scaleY: 1,
    },
    center:{x: bbox.cx, y: bbox.cy},
    size:{
        x: bbox.width,
        y: bbox.height
    }
}

var copy;

_drawBBoxGroup(path);

transform_translate = ()=>{
    transforms.translate.x += 5;
    transforms.translate.y += 5;    
    _apply();
}

transform_rotate = ()=>{    
    transforms.rotate += 5;
    _apply();
}

transform_scale = ()=>{
    transforms.scale.scaleX += 0.2;
    transforms.scale.scaleY += 0.2;     
    _apply();
}

var sin;
var cos;

// scaling
scale = (dx, dy, handle) =>{
    // dx e dy são as diferenças em relação ao ponto inicial
         
    // First rotate dx, dy to element alignment.
    var rx = dx * cos - dy * sin;
    var ry = dx * sin + dy * cos;

    rx *= Math.abs(handle.x);
    ry *= Math.abs(handle.y);

    // And finally rotate back to canvas alignment.
    var rdx = rx * cos + ry * sin;
    var rdy = rx * -sin + ry * cos;

    transforms.translate = {
        x: copy.translate.x + rdx / 2,
        y: copy.translate.y + rdy / 2
    };

    // get "handle" position
    var rad = {
        x: Snap.rad(copy.rotate),
        y: Snap.rad(copy.rotate + 90)
    };

    var radius = {
        x: copy.size.x / 2 * copy.scale.scaleX,
        y: copy.size.y / 2 * copy.scale.scaleY
    };
    
    var handlePos = {
        cx: (bbox.cx + handle.x * radius.x * Math.cos(rad.x)) + handle.y * radius.y * Math.cos(rad.y),
        cy: (bbox.cy + handle.x * radius.x * Math.sin(rad.x)) + handle.y * radius.y * Math.sin(rad.y)
    }

    //

    // Mouse position, relative to element center after translation.
    var mx = handlePos.cx + dx - transforms.center.x - transforms.translate.x;
    var my = handlePos.cy + dy - transforms.center.y - transforms.translate.y;

    // Position rotated to align with element.
    rx = mx * cos - my * sin;
    ry = mx * sin + my * cos;

    var isCorner = _isCorner(handle.x, handle.y);

    // Maintain aspect ratio
    if (isCorner) {
        var ratio = (transforms.size.x * transforms.scale.scaleX) / (transforms.size.y * transforms.scale.scaleY);
        var tdy = rx * handle.x * ( 1 / ratio );
        var tdx = ry * handle.y * ratio;

        if (tdx > tdy * ratio) {
            rx = tdx * handle.x;
        } else {
            ry = tdy * handle.y;
        }
    }

    // Scale element so that handle is at mouse position
    sx = rx * 2 * handle.x / copy.size.x;
    sy = ry * 2 * handle.y / copy.size.y;

    transforms.scale = {
        scaleX: Math.abs(sx || transforms.scale.scaleX),
        scaleY: Math.abs(sy || transforms.scale.scaleY)
    };

    // Maintain aspect ratio after scale
    if (isCorner) {
        if (tdx > tdy * ratio) {
            transforms.scale.scaleY = transforms.scale.scaleX / ratio;
        } else {
            transforms.scale.scaleX = transforms.scale.scaleY * ratio;
        }
        
        var trans = {
            x: (transforms.scale.scaleX - copy.scale.scaleX) * copy.size.x * handle.x,
            y: (transforms.scale.scaleY - copy.scale.scaleY) * copy.size.y * handle.y
        };

        rx = trans.x * cos + trans.y * sin;
        ry = -trans.x * sin + trans.y * cos;

        transforms.translate.x = copy.translate.x + rx / 2;
        transforms.translate.y = copy.translate.y + ry / 2;
    }

    _apply();

}

_startScale = ()=>{
    bbox = path.getBBox();
    var rotate = Snap.rad((360 - transforms.rotate) % 360)
    sin = Math.sin(rotate);
    cos = Math.cos(rotate);    

    copy = cloneObj(transforms);
}

_apply = ()=>{
    path.transform([
        'R' + transforms.rotate, transforms.center.x, transforms.center.y,
        'S' + transforms.scale.scaleX, transforms.scale.scaleY, transforms.center.x, transforms.center.y,
        'T' + transforms.translate.x, transforms.translate.y
    ].join());
 
    _drawBBoxGroup(path);

}


function _isCorner(x, y){
    return Math.abs(x) === Math.abs(y);
}

function cloneObj(obj) {
    var i, clone = {};
    for (i in obj) {
        clone[i] = typeof obj[i] === 'object' ? cloneObj(obj[i]) : obj[i];
    }
    return clone;
}

/*
    
*/


function _drawBBoxGroup(elem) {
    var SVG_NAMESPACE_URI = 'http://www.w3.org/2000/svg';
    
    //var bbox = elem.node.getBBox();
    var bbox = elem.getBBox();

    var container = document.getElementById("svg");


    var selectorGroup = document.getElementById('selectorGroup');
    selectorGroup.innerHTML = "";    

    
    var bboxCornersDirection = [
        {'name':'nw', 'x':-1, 'y':-1 },
        {'name':'ne', 'x': 1, 'y':-1 },
        {'name':'se', 'x': 1, 'y': 1 },
        {'name':'sw', 'x':-1, 'y': 1 },
    ];

    var bboxSidesDirection = [
        {'name':'n', 'x': 0, 'y':-1 },
        {'name':'e', 'x': 1, 'y': 0 },
        {'name':'s', 'x': 0, 'y': 1 },
        {'name':'w', 'x':-1, 'y': 0 },
    ];

    var handlesDirection = bboxCornersDirection.concat(bboxSidesDirection);
    var handles = [];

    // calc NW grip position
    var rad = {
        x: Snap.rad(transforms.rotate),
        y: Snap.rad(transforms.rotate + 90)
    };

    var radius = {
        x: transforms.size.x / 2 * transforms.scale.scaleX,
        y: transforms.size.y / 2 * transforms.scale.scaleY
    };
    
    var gripElem;
    var cx;
    var cy;
    for (var i = 0; i < handlesDirection.length; i++){
        gripElem = document.createElementNS(SVG_NAMESPACE_URI, 'circle');
        gripElem.style.cursor = 'nw-resize';
        gripElem.setAttribute('id', 'selectorGrip_' + handlesDirection[i].name);
        cx = (bbox.cx + handlesDirection[i].x * radius.x * Math.cos(rad.x)) + handlesDirection[i].y * radius.y * Math.cos(rad.y);
        cy = (bbox.cy + handlesDirection[i].x * radius.x * Math.sin(rad.x)) + handlesDirection[i].y * radius.y * Math.sin(rad.y);
        handles.push({
            'x' : cx,
            'y' : cy
        });

        gripElem.setAttribute('cx', cx);
        gripElem.setAttribute('cy', cy);
        gripElem.setAttribute('r', 4);   
        
        selectorGroup.appendChild(gripElem);
    }

    //

    // set box path
    var selectorBox = document.createElementNS(SVG_NAMESPACE_URI, 'path');
    selectorBox.setAttribute('fill', 'none');
    selectorBox.setAttribute('pointer-events', 'none');
    selectorBox.setAttribute('stroke', 'black');
    var boxPath = 'M' + handles[0].x + ',' +  handles[0].y +
                  'L' + handles[1].x + ',' +  handles[1].y +
                  'L' + handles[2].x + ',' +  handles[2].y +
                  'L' + handles[3].x + ',' +  handles[3].y +
                  'L' + handles[0].x + ',' +  handles[0].y;

    selectorBox.setAttribute('d', boxPath);        
    selectorGroup.appendChild(selectorBox);
    

    for (var i = 0; i < handlesDirection.length; i++){
        var _grip = document.getElementById('selectorGrip_' + handlesDirection[i].name);
        var _sGrip = Snap(_grip);
      
        (function (_handle){
            _sGrip.drag(function(dx, dy){
                scale(dx, dy, _handle);            
            },function(){
                _startScale();
            })
        })(handlesDirection[i]);
        
    }    

}