export default class Mediator {

    constructor() {
        this.channels = {};
    }

    subscribe(channel, context, func) {
        if (!this.channels[channel]) {
            this.channels[channel] = []
        }
        this.channels[channel].push({
            context : context,
            func : func
        });
    }

    publish(channel) {
        if (!this.channels[channel]) return false;
        
        let args = Array.prototype.slice.call(arguments,1);
        for(let i =0; i < this.channels[channel].length; i++) {

            let sub = this.channels[channel][i];
            sub.func.apply(sub.context, args);
        }
    }
}