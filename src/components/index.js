import TransformsService from './TransformsService';
import GripsService from './GripsService';

export const Components = {
    TransformsService: TransformsService,
    GripsService: GripsService
}