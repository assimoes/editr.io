
// listen for the apply event here and draw the grips here
export default class GripsService {

    constructor(snap, mediator, store) {
        this.mediator = mediator;
        this.store = store;
        this.Snap = snap;

        this.mediator.subscribe('scale_started', this, this._drawBBoxGroup);
        this.mediator.subscribe('transform_applied', this, this._drawBBoxGroup);
        this.mediator.subscribe('element_selected', this, this._drawBBoxGroup);
    }

    _drawBBoxGroup(elem) {
        var SVG_NAMESPACE_URI = 'http://www.w3.org/2000/svg';
        
        if (!this.store.elements[elem.id]) return false;
        
        let _elem = this.store.elements[elem.id];
        let _element = _elem.element;
        let _transforms = _elem.transforms;

        let copy = _elem.copy;

        //var bbox = elem.node.getBBox();
        let bbox = elem.getBBox();
    
        var container = document.getElementById("svg");
    
    
        var selectorGroup = document.getElementById('selectorGroup');
        selectorGroup.innerHTML = "";    
    
        
        var bboxCornersDirection = [
            {'name':'nw', 'x':-1, 'y':-1 },
            {'name':'ne', 'x': 1, 'y':-1 },
            {'name':'se', 'x': 1, 'y': 1 },
            {'name':'sw', 'x':-1, 'y': 1 },
        ];
    
        var bboxSidesDirection = [
            {'name':'n', 'x': 0, 'y':-1 },
            {'name':'e', 'x': 1, 'y': 0 },
            {'name':'s', 'x': 0, 'y': 1 },
            {'name':'w', 'x':-1, 'y': 0 },
        ];
    
        var handlesDirection = bboxCornersDirection.concat(bboxSidesDirection);
        var handles = [];
    
        // calc NW grip position
        var rad = {
            x: Snap.rad(_transforms.rotate),
            y: Snap.rad(_transforms.rotate + 90)
        };
    
        var radius = {
            x: _transforms.size.x / 2 * _transforms.scale.scaleX,
            y: _transforms.size.y / 2 * _transforms.scale.scaleY
        };
        
        var gripElem;
        var cx;
        var cy;
        for (var i = 0; i < handlesDirection.length; i++){
            gripElem = document.createElementNS(SVG_NAMESPACE_URI, 'circle');
            gripElem.style.cursor = 'nw-resize';
            gripElem.setAttribute('id', 'selectorGrip_' + handlesDirection[i].name);
            cx = (bbox.cx + handlesDirection[i].x * radius.x * Math.cos(rad.x)) + handlesDirection[i].y * radius.y * Math.cos(rad.y);
            cy = (bbox.cy + handlesDirection[i].x * radius.x * Math.sin(rad.x)) + handlesDirection[i].y * radius.y * Math.sin(rad.y);
            handles.push({
                'x' : cx,
                'y' : cy
            });
    
            gripElem.setAttribute('cx', cx);
            gripElem.setAttribute('cy', cy);
            gripElem.setAttribute('r', 4);   
            
            selectorGroup.appendChild(gripElem);
        }
    
        //
    
        // set box path
        var selectorBox = document.createElementNS(SVG_NAMESPACE_URI, 'path');
        selectorBox.setAttribute('fill', 'none');
        selectorBox.setAttribute('pointer-events', 'none');
        selectorBox.setAttribute('stroke', 'black');
        var boxPath = 'M' + handles[0].x + ',' +  handles[0].y +
                      'L' + handles[1].x + ',' +  handles[1].y +
                      'L' + handles[2].x + ',' +  handles[2].y +
                      'L' + handles[3].x + ',' +  handles[3].y +
                      'L' + handles[0].x + ',' +  handles[0].y;
    
        selectorBox.setAttribute('d', boxPath);        
        selectorGroup.appendChild(selectorBox);
        
    
        for (var i = 0; i < handlesDirection.length; i++){
            var _grip = document.getElementById('selectorGrip_' + handlesDirection[i].name);
            var _sGrip = Snap(_grip);
            let _self = this;
            (function (_handle){
                _sGrip.drag(function(dx, dy){
                    
                    _self.mediator.publish('dragging',dx, dy, elem, _handle);
                    //scale(dx, dy, _handle);          
                },function(){
                    _self.mediator.publish('drag_started', elem);
                    //_startScale();
                })
            })(handlesDirection[i]);
            
        }    
    
    }
}