export default class TransformsService {

    constructor(snap, mediator, store) {
        this.mediator = mediator;
        this.store = store;
        this.Snap = snap;
        this.mediator.subscribe('drag_started', this, this.StartScale);
        this.mediator.subscribe('dragging', this, this.Scale);
        this.mediator.subscribe('drag_ended', this, this.EndScale);
        this.mediator.subscribe('test', this, this.test);
    }

    Load(elems) {
        elems.array.forEach(element => {
            if (!this.store.elements[element.id]) {
                this.store.elements[element.id] = {
                    transforms : {
                        rotate : 0,
                        translate: {
                            x: 0,
                            y: 0
                        },
                        scale: {
                            scaleX: 1,
                            scaleY: 1,
                        },
                        center:{x: bbox.cx, y: bbox.cy},
                        size:{
                            x: bbox.width,
                            y: bbox.height
                        }
                    },
                    copy: {},
                    sin: -1,
                    cos: -1,
                    element: elem
                }
            }
        });
    }

    NewElement(elem) {

        if (!this.store.elements[elem.id]) return false;
        let _elem = this.elements[elem.id];
        let _element = _elem.element;
        let _transforms = _elem.transforms;

        if (!this.store.elements[elem.id]) {
            this.store.elements[elem.id] = {

                transforms : {
                    rotate : 0,
                    translate: {
                        x: 0,
                        y: 0
                    },
                    scale: {
                        scaleX: 1,
                        scaleY: 1,
                    },
                    center:{x: _elem.bbox.cx, y: _elem.bbox.cy},
                    size:{
                        x: _elem.bbox.width,
                        y: _elem.bbox.height
                    }
                },
                copy: {},
                sin: -1,
                cos: -1,
                element: elem,
                bbox: {}
            }
        }
    }

    Scale(dx, dy, elems, handle) {
        
        if (elems instanceof Array) {
            elems.forEach((elem)=> {
                this._scale(dx, dy, elem, handle);
            });
        } else {
            this._scale(dx,dy,elems, handle);
        }
    }

    test() {
        this.store.test = 'test';
        console.log('test');
    }


    StartScale(elem) {
        if (!this.store.elements[elem.id]) return false;
        let _elem = this.store.elements[elem.id];
        let _element = _elem.element;
        let _transforms = _elem.transforms;

        _elem.bbox = _element.getBBox();
        let rotate = this.Snap.rad((360 - _transforms.rotate) % 360);
        _elem.sin = Math.sin(rotate);
        _elem.cos = Math.cos(rotate);    
        _elem.copy = this._cloneObj(_transforms);

        this.mediator.publish('scale_started', elem);
        //this._drawBBoxGroup(elem);
    }

    EndScale() {
        console.log('end scale')
    }

    _apply(elem){
        
        if (!this.store.elements[elem.id]) return false;
        let _elem = this.store.elements[elem.id];
        let _element = _elem.element;
        let _transforms = _elem.transforms;
        
        _element.transform([
            'R' + _elem.transforms.rotate, _elem.transforms.center.x, _elem.transforms.center.y,
            'S' + _elem.transforms.scale.scaleX, _elem.transforms.scale.scaleY, _elem.transforms.center.x, _elem.transforms.center.y,
            'T' + _elem.transforms.translate.x, _elem.transforms.translate.y
        ].join());
     
        this.mediator.publish('transform_applied', elem);
    }

    _scale(dx, dy, elem, handle) {
        if (!this.store.elements[elem.id]) return false;
        
        let _elem = this.store.elements[elem.id];
        let _element = _elem.element;
        let _transforms = _elem.transforms;

        let copy = _elem.copy;

        let rx = dx * _elem.cos - dy * _elem.sin;
        let ry = dx * _elem.sin + dy * _elem.cos;
    
        rx *= Math.abs(handle.x);
        ry *= Math.abs(handle.y);
    
        let rdx = rx * _elem.cos + ry * _elem.sin;
        let rdy = rx * -_elem.sin + ry * _elem.cos;
    
        _elem.transforms.translate = {
            x: copy.translate.x + rdx / 2,
            y: copy.translate.y + rdy / 2
        };

        let rad = {
            x: this.Snap.rad(copy.rotate),
            y: this.Snap.rad(copy.rotate + 90)
        };
    
        let radius = {
            x: copy.size.x / 2 * copy.scale.scaleX,
            y: copy.size.y / 2 * copy.scale.scaleY
        };
        
        let handlePos = {
            cx: (_elem.bbox.cx + handle.x * radius.x * Math.cos(rad.x)) + handle.y * radius.y * Math.cos(rad.y),
            cy: (_elem.bbox.cy + handle.x * radius.x * Math.sin(rad.x)) + handle.y * radius.y * Math.sin(rad.y)
        }
        
        // Mouse position, relative to element center after translation.
        let mx = handlePos.cx + dx - _elem.transforms.center.x - _elem.transforms.translate.x;
        let my = handlePos.cy + dy - _elem.transforms.center.y - _elem.transforms.translate.y;

        // Position rotated to align with element.
        rx = mx * _elem.cos - my * _elem.sin;
        ry = mx * _elem.sin + my * _elem.cos;
        
        // Scale element so that handle is at mouse position
        let sx = rx * 2 * handle.x / copy.size.x;
        let sy = ry * 2 * handle.y / copy.size.y;

        _transforms.scale = {
            scaleX: Math.abs(sx || _elem.transforms.scale.scaleX),
            scaleY: Math.abs(sy || _elem.transforms.scale.scaleY)
        };


        this._apply(elem)

    }

    _cloneObj(obj) {
        let i, clone = {};
        for (i in obj) {
            clone[i] = typeof obj[i] === 'object' ? this._cloneObj(obj[i]) : obj[i];
        }
        return clone;
    }
}